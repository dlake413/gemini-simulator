﻿namespace GeminiSimulator
{
    partial class GeminiSimuilatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headlabel = new System.Windows.Forms.Label();
            this.LoadButton = new System.Windows.Forms.Button();
            this.ReloadButton = new System.Windows.Forms.Button();
            this.ResetCPUButton = new System.Windows.Forms.Button();
            this.LoadDirecLabel = new System.Windows.Forms.Label();
            this.LoadDirectory = new System.Windows.Forms.Label();
            this.Instruct1 = new System.Windows.Forms.Label();
            this.Instruct2 = new System.Windows.Forms.Label();
            this.CurrentInstructionLabel = new System.Windows.Forms.Label();
            this.NextInstructionLabel = new System.Windows.Forms.Label();
            this.MemoryPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RegCell = new System.Windows.Forms.TableLayoutPanel();
            this.CCRegLabel = new System.Windows.Forms.Label();
            this.ARegLabel = new System.Windows.Forms.Label();
            this.BRegLabel = new System.Windows.Forms.Label();
            this.ACCRegLabel = new System.Windows.Forms.Label();
            this.ZERORegLabel = new System.Windows.Forms.Label();
            this.ONERegLabel = new System.Windows.Forms.Label();
            this.PCRegLabel = new System.Windows.Forms.Label();
            this.MARRegLabel = new System.Windows.Forms.Label();
            this.MDRRegLabel = new System.Windows.Forms.Label();
            this.TEMPRegLabel = new System.Windows.Forms.Label();
            this.IRRegLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NextInst = new System.Windows.Forms.Button();
            this.CacheBox = new System.Windows.Forms.TextBox();
            this.cpuSetting = new System.Windows.Forms.GroupBox();
            this.associativity = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.blockSize = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cacheSize = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.histMode = new System.Windows.Forms.CheckBox();
            this.bpMode = new System.Windows.Forms.CheckBox();
            this.pMode = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PipeConsole = new System.Windows.Forms.TextBox();
            this.MemoryPanel.SuspendLayout();
            this.RegCell.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.cpuSetting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // headlabel
            // 
            this.headlabel.AutoSize = true;
            this.headlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headlabel.Location = new System.Drawing.Point(12, 9);
            this.headlabel.Name = "headlabel";
            this.headlabel.Size = new System.Drawing.Size(122, 18);
            this.headlabel.TabIndex = 0;
            this.headlabel.Text = "Gemini Simulator";
            // 
            // LoadButton
            // 
            this.LoadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadButton.Location = new System.Drawing.Point(163, 7);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(89, 23);
            this.LoadButton.TabIndex = 1;
            this.LoadButton.Text = "Load File";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // ReloadButton
            // 
            this.ReloadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReloadButton.Location = new System.Drawing.Point(258, 7);
            this.ReloadButton.Name = "ReloadButton";
            this.ReloadButton.Size = new System.Drawing.Size(89, 23);
            this.ReloadButton.TabIndex = 2;
            this.ReloadButton.Text = "Reload";
            this.ReloadButton.UseVisualStyleBackColor = true;
            this.ReloadButton.Click += new System.EventHandler(this.ReloadButton_Click);
            // 
            // ResetCPUButton
            // 
            this.ResetCPUButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetCPUButton.Location = new System.Drawing.Point(353, 7);
            this.ResetCPUButton.Name = "ResetCPUButton";
            this.ResetCPUButton.Size = new System.Drawing.Size(89, 23);
            this.ResetCPUButton.TabIndex = 3;
            this.ResetCPUButton.Text = "Reset CPU";
            this.ResetCPUButton.UseVisualStyleBackColor = true;
            this.ResetCPUButton.Click += new System.EventHandler(this.ResetCPUButton_Click);
            // 
            // LoadDirecLabel
            // 
            this.LoadDirecLabel.AutoSize = true;
            this.LoadDirecLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadDirecLabel.Location = new System.Drawing.Point(12, 48);
            this.LoadDirecLabel.Name = "LoadDirecLabel";
            this.LoadDirecLabel.Size = new System.Drawing.Size(81, 16);
            this.LoadDirecLabel.TabIndex = 4;
            this.LoadDirecLabel.Text = "Current File: ";
            // 
            // LoadDirectory
            // 
            this.LoadDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadDirectory.Location = new System.Drawing.Point(99, 48);
            this.LoadDirectory.Name = "LoadDirectory";
            this.LoadDirectory.Size = new System.Drawing.Size(343, 23);
            this.LoadDirectory.TabIndex = 5;
            // 
            // Instruct1
            // 
            this.Instruct1.AutoSize = true;
            this.Instruct1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.Instruct1.Location = new System.Drawing.Point(12, 95);
            this.Instruct1.Name = "Instruct1";
            this.Instruct1.Size = new System.Drawing.Size(108, 16);
            this.Instruct1.TabIndex = 6;
            this.Instruct1.Text = "Instruction Index: ";
            // 
            // Instruct2
            // 
            this.Instruct2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.Instruct2.Location = new System.Drawing.Point(12, 129);
            this.Instruct2.Name = "Instruct2";
            this.Instruct2.Size = new System.Drawing.Size(108, 16);
            this.Instruct2.TabIndex = 7;
            this.Instruct2.Text = "Next Instruction:";
            // 
            // CurrentInstructionLabel
            // 
            this.CurrentInstructionLabel.Font = new System.Drawing.Font("Consolas", 9.25F);
            this.CurrentInstructionLabel.Location = new System.Drawing.Point(126, 95);
            this.CurrentInstructionLabel.Name = "CurrentInstructionLabel";
            this.CurrentInstructionLabel.Size = new System.Drawing.Size(134, 23);
            this.CurrentInstructionLabel.TabIndex = 8;
            this.CurrentInstructionLabel.Text = "-------- / -";
            // 
            // NextInstructionLabel
            // 
            this.NextInstructionLabel.Font = new System.Drawing.Font("Consolas", 9.25F);
            this.NextInstructionLabel.Location = new System.Drawing.Point(126, 129);
            this.NextInstructionLabel.Name = "NextInstructionLabel";
            this.NextInstructionLabel.Size = new System.Drawing.Size(134, 23);
            this.NextInstructionLabel.TabIndex = 9;
            this.NextInstructionLabel.Text = "--------";
            // 
            // MemoryPanel
            // 
            this.MemoryPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MemoryPanel.ColumnCount = 2;
            this.MemoryPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.33333F));
            this.MemoryPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.66667F));
            this.MemoryPanel.Controls.Add(this.RegCell, 1, 1);
            this.MemoryPanel.Controls.Add(this.label1, 0, 0);
            this.MemoryPanel.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.MemoryPanel.Controls.Add(this.label2, 1, 0);
            this.MemoryPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.MemoryPanel.Location = new System.Drawing.Point(12, 155);
            this.MemoryPanel.Name = "MemoryPanel";
            this.MemoryPanel.RowCount = 2;
            this.MemoryPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.MemoryPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 253F));
            this.MemoryPanel.Size = new System.Drawing.Size(248, 290);
            this.MemoryPanel.TabIndex = 11;
            this.MemoryPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.MemoryPanel_Paint);
            // 
            // RegCell
            // 
            this.RegCell.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.RegCell.ColumnCount = 1;
            this.RegCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RegCell.Controls.Add(this.CCRegLabel, 0, 10);
            this.RegCell.Controls.Add(this.ARegLabel, 0, 0);
            this.RegCell.Controls.Add(this.BRegLabel, 0, 1);
            this.RegCell.Controls.Add(this.ACCRegLabel, 0, 2);
            this.RegCell.Controls.Add(this.ZERORegLabel, 0, 3);
            this.RegCell.Controls.Add(this.ONERegLabel, 0, 4);
            this.RegCell.Controls.Add(this.PCRegLabel, 0, 5);
            this.RegCell.Controls.Add(this.MARRegLabel, 0, 6);
            this.RegCell.Controls.Add(this.MDRRegLabel, 0, 7);
            this.RegCell.Controls.Add(this.TEMPRegLabel, 0, 8);
            this.RegCell.Controls.Add(this.IRRegLabel, 0, 9);
            this.RegCell.Font = new System.Drawing.Font("Consolas", 9.25F);
            this.RegCell.Location = new System.Drawing.Point(108, 25);
            this.RegCell.Margin = new System.Windows.Forms.Padding(0);
            this.RegCell.Name = "RegCell";
            this.RegCell.RowCount = 11;
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.RegCell.Size = new System.Drawing.Size(139, 264);
            this.RegCell.TabIndex = 12;
            this.RegCell.Paint += new System.Windows.Forms.PaintEventHandler(this.RegCell_Paint);
            // 
            // CCRegLabel
            // 
            this.CCRegLabel.Location = new System.Drawing.Point(4, 241);
            this.CCRegLabel.Name = "CCRegLabel";
            this.CCRegLabel.Size = new System.Drawing.Size(131, 23);
            this.CCRegLabel.TabIndex = 12;
            this.CCRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ARegLabel
            // 
            this.ARegLabel.Location = new System.Drawing.Point(4, 1);
            this.ARegLabel.Name = "ARegLabel";
            this.ARegLabel.Size = new System.Drawing.Size(131, 23);
            this.ARegLabel.TabIndex = 4;
            this.ARegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BRegLabel
            // 
            this.BRegLabel.Location = new System.Drawing.Point(4, 25);
            this.BRegLabel.Name = "BRegLabel";
            this.BRegLabel.Size = new System.Drawing.Size(131, 23);
            this.BRegLabel.TabIndex = 5;
            this.BRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ACCRegLabel
            // 
            this.ACCRegLabel.Location = new System.Drawing.Point(4, 49);
            this.ACCRegLabel.Name = "ACCRegLabel";
            this.ACCRegLabel.Size = new System.Drawing.Size(131, 23);
            this.ACCRegLabel.TabIndex = 6;
            this.ACCRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ZERORegLabel
            // 
            this.ZERORegLabel.Location = new System.Drawing.Point(4, 73);
            this.ZERORegLabel.Name = "ZERORegLabel";
            this.ZERORegLabel.Size = new System.Drawing.Size(131, 23);
            this.ZERORegLabel.TabIndex = 7;
            this.ZERORegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ONERegLabel
            // 
            this.ONERegLabel.Location = new System.Drawing.Point(4, 97);
            this.ONERegLabel.Name = "ONERegLabel";
            this.ONERegLabel.Size = new System.Drawing.Size(131, 23);
            this.ONERegLabel.TabIndex = 8;
            this.ONERegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PCRegLabel
            // 
            this.PCRegLabel.Location = new System.Drawing.Point(4, 121);
            this.PCRegLabel.Name = "PCRegLabel";
            this.PCRegLabel.Size = new System.Drawing.Size(131, 23);
            this.PCRegLabel.TabIndex = 9;
            this.PCRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MARRegLabel
            // 
            this.MARRegLabel.Location = new System.Drawing.Point(4, 145);
            this.MARRegLabel.Name = "MARRegLabel";
            this.MARRegLabel.Size = new System.Drawing.Size(131, 23);
            this.MARRegLabel.TabIndex = 10;
            this.MARRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MDRRegLabel
            // 
            this.MDRRegLabel.Location = new System.Drawing.Point(4, 169);
            this.MDRRegLabel.Name = "MDRRegLabel";
            this.MDRRegLabel.Size = new System.Drawing.Size(131, 23);
            this.MDRRegLabel.TabIndex = 11;
            this.MDRRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TEMPRegLabel
            // 
            this.TEMPRegLabel.Location = new System.Drawing.Point(4, 193);
            this.TEMPRegLabel.Name = "TEMPRegLabel";
            this.TEMPRegLabel.Size = new System.Drawing.Size(131, 23);
            this.TEMPRegLabel.TabIndex = 12;
            this.TEMPRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IRRegLabel
            // 
            this.IRRegLabel.Location = new System.Drawing.Point(4, 217);
            this.IRRegLabel.Name = "IRRegLabel";
            this.IRRegLabel.Size = new System.Drawing.Size(131, 23);
            this.IRRegLabel.TabIndex = 13;
            this.IRRegLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Register";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 9);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 25);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(106, 264);
            this.tableLayoutPanel1.TabIndex = 2;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(4, 241);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 23);
            this.label13.TabIndex = 13;
            this.label13.Text = "CC";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "A";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "B";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 23);
            this.label5.TabIndex = 5;
            this.label5.Text = "ACC";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(4, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 23);
            this.label6.TabIndex = 6;
            this.label6.Text = "ZERO";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(4, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 23);
            this.label7.TabIndex = 7;
            this.label7.Text = "ONE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(4, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 23);
            this.label8.TabIndex = 8;
            this.label8.Text = "PC";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(4, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 23);
            this.label9.TabIndex = 9;
            this.label9.Text = "MAR";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(4, 169);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 23);
            this.label10.TabIndex = 10;
            this.label10.Text = "MDR";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(4, 193);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 23);
            this.label11.TabIndex = 11;
            this.label11.Text = "TEMP";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(4, 217);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 23);
            this.label12.TabIndex = 12;
            this.label12.Text = "IR";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(111, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Value";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NextInst
            // 
            this.NextInst.Location = new System.Drawing.Point(293, 92);
            this.NextInst.Name = "NextInst";
            this.NextInst.Size = new System.Drawing.Size(90, 23);
            this.NextInst.TabIndex = 13;
            this.NextInst.Text = "Next Instruction";
            this.NextInst.UseVisualStyleBackColor = true;
            this.NextInst.Click += new System.EventHandler(this.NextInst_Click);
            // 
            // CacheBox
            // 
            this.CacheBox.BackColor = System.Drawing.SystemColors.Window;
            this.CacheBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CacheBox.Location = new System.Drawing.Point(11, 451);
            this.CacheBox.Multiline = true;
            this.CacheBox.Name = "CacheBox";
            this.CacheBox.ReadOnly = true;
            this.CacheBox.Size = new System.Drawing.Size(435, 85);
            this.CacheBox.TabIndex = 14;
            // 
            // cpuSetting
            // 
            this.cpuSetting.Controls.Add(this.associativity);
            this.cpuSetting.Controls.Add(this.label16);
            this.cpuSetting.Controls.Add(this.blockSize);
            this.cpuSetting.Controls.Add(this.label15);
            this.cpuSetting.Controls.Add(this.label14);
            this.cpuSetting.Controls.Add(this.cacheSize);
            this.cpuSetting.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpuSetting.Location = new System.Drawing.Point(266, 155);
            this.cpuSetting.Name = "cpuSetting";
            this.cpuSetting.Size = new System.Drawing.Size(180, 154);
            this.cpuSetting.TabIndex = 15;
            this.cpuSetting.TabStop = false;
            this.cpuSetting.Text = "CPU Setting";
            // 
            // associativity
            // 
            this.associativity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.associativity.FormattingEnabled = true;
            this.associativity.Items.AddRange(new object[] {
            "Direct Mapping",
            "2-Way Associative"});
            this.associativity.Location = new System.Drawing.Point(6, 120);
            this.associativity.Name = "associativity";
            this.associativity.Size = new System.Drawing.Size(167, 23);
            this.associativity.TabIndex = 5;
            this.associativity.SelectedIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.label16.Location = new System.Drawing.Point(6, 103);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Associativity";
            // 
            // blockSize
            // 
            this.blockSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockSize.FormattingEnabled = true;
            this.blockSize.Items.AddRange(new object[] {
            "1",
            "2"});
            this.blockSize.Location = new System.Drawing.Point(6, 74);
            this.blockSize.Name = "blockSize";
            this.blockSize.Size = new System.Drawing.Size(167, 23);
            this.blockSize.TabIndex = 3;
            this.blockSize.SelectedIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.label15.Location = new System.Drawing.Point(6, 60);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Block Size";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.label14.Location = new System.Drawing.Point(6, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Cache Size";
            // 
            // cacheSize
            // 
            this.cacheSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cacheSize.FormattingEnabled = true;
            this.cacheSize.ItemHeight = 15;
            this.cacheSize.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "8",
            "16"});
            this.cacheSize.Location = new System.Drawing.Point(6, 32);
            this.cacheSize.Name = "cacheSize";
            this.cacheSize.Size = new System.Drawing.Size(167, 23);
            this.cacheSize.TabIndex = 0;
            this.cacheSize.SelectedIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pMode);
            this.groupBox1.Controls.Add(this.bpMode);
            this.groupBox1.Controls.Add(this.histMode);
            this.groupBox1.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.groupBox1.Location = new System.Drawing.Point(266, 315);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 105);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pipeline Setting";
            // 
            // histMode
            // 
            this.histMode.AutoSize = true;
            this.histMode.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.histMode.Location = new System.Drawing.Point(6, 47);
            this.histMode.Name = "histMode";
            this.histMode.Size = new System.Drawing.Size(140, 17);
            this.histMode.TabIndex = 2;
            this.histMode.Text = "Local History Table";
            this.histMode.UseVisualStyleBackColor = true;
            // 
            // bpMode
            // 
            this.bpMode.AutoSize = true;
            this.bpMode.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.bpMode.Location = new System.Drawing.Point(6, 70);
            this.bpMode.Name = "bpMode";
            this.bpMode.Size = new System.Drawing.Size(80, 17);
            this.bpMode.TabIndex = 3;
            this.bpMode.Text = "Bypassing";
            this.bpMode.UseVisualStyleBackColor = true;
            // 
            // pMode
            // 
            this.pMode.AutoSize = true;
            this.pMode.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.pMode.Location = new System.Drawing.Point(6, 22);
            this.pMode.Name = "pMode";
            this.pMode.Size = new System.Drawing.Size(116, 17);
            this.pMode.TabIndex = 4;
            this.pMode.Text = "Enable Pipeline";
            this.pMode.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PipeConsole);
            this.panel1.Location = new System.Drawing.Point(462, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(451, 524);
            this.panel1.TabIndex = 17;
            // 
            // PipeConsole
            // 
            this.PipeConsole.Location = new System.Drawing.Point(6, 3);
            this.PipeConsole.Multiline = true;
            this.PipeConsole.Name = "PipeConsole";
            this.PipeConsole.Size = new System.Drawing.Size(442, 518);
            this.PipeConsole.TabIndex = 0;
            // 
            // GeminiSimuilatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 548);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cpuSetting);
            this.Controls.Add(this.CacheBox);
            this.Controls.Add(this.NextInst);
            this.Controls.Add(this.MemoryPanel);
            this.Controls.Add(this.NextInstructionLabel);
            this.Controls.Add(this.CurrentInstructionLabel);
            this.Controls.Add(this.Instruct2);
            this.Controls.Add(this.Instruct1);
            this.Controls.Add(this.LoadDirectory);
            this.Controls.Add(this.LoadDirecLabel);
            this.Controls.Add(this.ResetCPUButton);
            this.Controls.Add(this.ReloadButton);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.headlabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "GeminiSimuilatorForm";
            this.Text = "Gemini Simulator";
            this.Load += new System.EventHandler(this.GeminiSimuilatorForm_Load);
            this.MemoryPanel.ResumeLayout(false);
            this.RegCell.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.cpuSetting.ResumeLayout(false);
            this.cpuSetting.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label headlabel;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button ReloadButton;
        private System.Windows.Forms.Button ResetCPUButton;
        private System.Windows.Forms.Label LoadDirecLabel;
        private System.Windows.Forms.Label LoadDirectory;
        private System.Windows.Forms.Label Instruct1;
        private System.Windows.Forms.Label Instruct2;
        private System.Windows.Forms.Label CurrentInstructionLabel;
        private System.Windows.Forms.Label NextInstructionLabel;
        private System.Windows.Forms.TableLayoutPanel MemoryPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel RegCell;
        private System.Windows.Forms.Label ARegLabel;
        private System.Windows.Forms.Label BRegLabel;
        private System.Windows.Forms.Label ACCRegLabel;
        private System.Windows.Forms.Label ZERORegLabel;
        private System.Windows.Forms.Label CCRegLabel;
        private System.Windows.Forms.Label ONERegLabel;
        private System.Windows.Forms.Label PCRegLabel;
        private System.Windows.Forms.Label MARRegLabel;
        private System.Windows.Forms.Label MDRRegLabel;
        private System.Windows.Forms.Label TEMPRegLabel;
        private System.Windows.Forms.Label IRRegLabel;
        private System.Windows.Forms.Button NextInst;
        private System.Windows.Forms.TextBox CacheBox;
        private System.Windows.Forms.GroupBox cpuSetting;
        private System.Windows.Forms.ComboBox cacheSize;
        private System.Windows.Forms.ComboBox blockSize;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox associativity;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox pMode;
        private System.Windows.Forms.CheckBox bpMode;
        private System.Windows.Forms.CheckBox histMode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox PipeConsole;
    }
}

