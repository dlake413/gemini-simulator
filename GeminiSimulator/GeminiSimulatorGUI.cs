﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using GeminiCore;
using System.IO;

namespace GeminiSimulator
{
    public partial class GeminiSimuilatorForm : Form
    {
        public CPU cpu;
        public Memory mem;
        public string performance = "";
        private bool loaded = false;
        #region ISA DEFINITION
        public Dictionary<string, byte> ISA = new Dictionary<string, byte>(){
            {"LDA", (byte)1},
            {"STA", (byte)2},
            {"ADD", (byte)4},
            {"SUB", (byte)8},
            {"MUL", (byte)16},
            {"DIV", (byte)24},
            {"AND", (byte)12},
            {"OR", (byte)6},
            {"SHL", (byte)3},
            {"NOTA", (byte)20},
            {"BA", (byte)10},
            {"BE", (byte)5},
            {"BL", (byte)18},
            {"BG", (byte)9},
            {"NOP", (byte)17},
            {"HLT", (byte)7},
        };
        #endregion
        public GeminiSimuilatorForm()
        {
            InitializeComponent();
        }
        #region ClickEvents
        private void LoadButton_Click(object sender, EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Filter = "Assembly Files (.s)|*.s";
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var result = new IPE(ofd.FileName);
                    try
                    {
                        result.ParseFile();
                        LoadDirectory.Text = result.FileToParse;
                        this.loaded = true;
                        initializeSimulator();
                        foreach (short inst in cpu.getMemory().getInstructions())
                        {
                            Console.Write(inst.ToString("X4") + " ");
                        }
                        Console.Write("\n");
                        DisplayRegisters();
                    }
                    catch (SyntaxError error)
                    {
                        result.forceClosegout();
                        MessageBox.Show("Syntax error found at index: " + error.Message + "\nFile will not be loaded.", "SyntaxError", MessageBoxButtons.OK);
                    }
                    catch (InternalBufferOverflowException error)
                    {
                        result.forceClosegout();
                        MessageBox.Show("Buffer overflow error found at index: " + error.Message + "\nFile will not be loaded.", "BufferOverflowError", MessageBoxButtons.OK);
                    }
                    catch (LabelAlreadyExist error)
                    {
                        result.forceClosegout();
                        MessageBox.Show(error.Message + "\nFile will be not loaded.", "LabelConflict", MessageBoxButtons.OK);
                    }
                };
            };
        }
        #endregion

        //initializeSimulator will read g.out file created during load section.
        public void initializeSimulator()
        {
            performance = "";
            string goutdirectory = Regex.Replace(LoadDirectory.Text, @"([\d\w]*\.(.*))$", "g.out");
            BinaryReader goutreader = new BinaryReader(File.Open(goutdirectory,FileMode.Open));
            List<short> instructions = new List<short>();
            while(goutreader.BaseStream.Position != goutreader.BaseStream.Length)
            { 
                short instruct = BitConverter.ToInt16(goutreader.ReadBytes(2), 0);
                instructions.Add(instruct);
            }
            this.cpu = new CPU();
            this.cpu.initializeMemory(instructions);
            bool assoc = this.associativity.SelectedIndex == 1;
            this.cpu.initializeCache(Convert.ToInt16(blockSize.SelectedItem.ToString()), Convert.ToInt16(cacheSize.SelectedItem.ToString()),assoc);
            this.mem = cpu.getMemory();
            if (this.pMode.Checked)
            {
                cpu.pMode = true;
                if (this.bpMode.Checked)
                {
                    cpu.bMode = true;
                }
                if (this.histMode.Checked)
                {
                    cpu.lhMode = true;
                }
                cpu.pipelinedOperation();
            }
            this.CacheBox.Text = this.associativity.Text + " Initialized...";
        }

        //UPDATE Register Texts
        private void DisplayRegisters()
        {
            short nextPC = ((short)(cpu.PC +4)) ;
            this.ARegLabel.Text = ShortToByteConvert(cpu.A);
            this.BRegLabel.Text = ShortToByteConvert(cpu.B);
            this.ACCRegLabel.Text = ShortToByteConvert(cpu.ACC);
            this.PCRegLabel.Text = ShortToByteConvert(cpu.PC);
            this.ZERORegLabel.Text = ShortToByteConvert(cpu.ZERO);
            this.ONERegLabel.Text = ShortToByteConvert(cpu.ONE);
            this.IRRegLabel.Text = ShortToByteConvert(cpu.IR);
            this.MARRegLabel.Text = ShortToByteConvert(cpu.MAR);
            this.MDRRegLabel.Text = ShortToByteConvert(cpu.MDR);
            this.TEMPRegLabel.Text = ShortToByteConvert(cpu.TEMP);
            this.CCRegLabel.Text = ShortToByteConvert(cpu.CC);
            this.CurrentInstructionLabel.Text = OpcodeFinder(cpu.getInstruction());
            if (mem.getInstructions().Count > (nextPC / 2))
            {
                this.NextInstructionLabel.Text = OpcodeFinder(mem.getInstruction(nextPC / 2),true);
            }
            else { this.NextInstructionLabel.Text = "....."; }
        }

        //Finds string representaion of the opcode given instruction number
        //Duke Park: Added the memory address/immediate value line + index
        private string OpcodeFinder(short input, bool next = false)
        {
            byte[] instruction = BitConverter.GetBytes(input);
            string address = ((int)instruction[1] % 2 == 1) ? "#$" : " $";
            address += instruction[0].ToString("D3");
            byte opcode = (byte)((int)instruction[1] >> 1);
            string opcodestring;
            if (!next)
            {
                opcodestring = String.Format("{0,-4}{1,-4} / {2,-4}", ISA.First(x => x.Value.Equals(opcode)).Key, address, cpu.PC/4 + 1);
            }
            else
            {
                opcodestring = String.Format("{0,-4}{1,-4}", ISA.First(x => x.Value.Equals(opcode)).Key, address);
            }
            return opcodestring;
        }

        // Just to make code simpler
        private string ShortToByteConvert(int input)
        {
            byte[] bytearray = BitConverter.GetBytes(input);
            string bytestring = BitConverter.ToString(bytearray);
            bytestring = "0x" + String.Join("", bytestring.Split('-'));
            return bytestring;
        }

        private void GeminiSimuilatorForm_Load(object sender, EventArgs e)
        {

        }

        private void MemoryPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RegCell_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ReloadButton_Click(object sender, EventArgs e)
        {
            if (this.loaded)
            {
                var result = new IPE(this.LoadDirectory.Text);
                try
                {
                    result.ParseFile();
                    initializeSimulator();
                    //foreach (short inst in cpu.getMemory().getInstructions())
                    //{
                    //    Console.Write(inst.ToString("X4") + " ");
                    //}
                    DisplayRegisters();
                }
                catch (SyntaxError error)
                {
                    result.forceClosegout();
                    MessageBox.Show("Syntax error found at index: " + error.Message + "\nFile will be not loaded.", "SyntaxError", MessageBoxButtons.OK);
                }
                catch (InternalBufferOverflowException error)
                {
                    result.forceClosegout();
                    MessageBox.Show("Buffer overflow error found at index: " + error.Message + "\nFile will be not loaded.", "BufferOverflowError", MessageBoxButtons.OK);
                }
                catch (LabelAlreadyExist error)
                {
                    result.forceClosegout();
                    MessageBox.Show(error.Message + "\nFile will be not loaded.", "LabelConflict", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("File is not loaded.","Warning",MessageBoxButtons.OK);
            }
        }

        private void ResetCPUButton_Click(object sender, EventArgs e)
        {
            if (this.loaded)
            {
                this.cpu.initializeCPU();
                if (pMode.Checked)
                {
                    cpu.pMode = true;
                    if (this.bpMode.Checked)
                    {
                        cpu.bMode = true;
                    }
                    if (this.histMode.Checked)
                    {
                        cpu.lhMode = true;
                    }
                }
                bool assoc = this.associativity.Text == "2-Way Associative";
                this.cpu.initializeCache(Convert.ToInt16(blockSize.SelectedItem.ToString()),Convert.ToInt16(cacheSize.SelectedItem.ToString()),assoc);
                this.mem.initializeMemory();
                DisplayRegisters();
            }
            else
            {
                MessageBox.Show("File is not loaded.", "Warning", MessageBoxButtons.OK);
            }
        }

        private void NextInst_Click(object sender, EventArgs e)
        {
            if (this.loaded)
            {
                try
                {
                    if (this.cpu.pMode)
                    {
                        Console.WriteLine();
                        Console.WriteLine("=============================");
                        //pipeline begins
                        this.cpu.pipelinedOperation();
                        DisplayRegisters();
                    }
                    else
                    {
                        this.cpu.operate();
                        DisplayRegisters();
                    }
                    string message = cpu.getCacheMessage();
                    if (message != "")
                    {
                        this.CacheBox.AppendText(message + "\r\n");
                    }
                }
                catch (CPUHalted)
                {
                    MessageBox.Show("All instructions are completed.", "Halted", MessageBoxButtons.OK);
                    if (this.performance == "")
                    {
                        performance = "\r\nPERFORMANCE RESULT: \r\nNUMBER OF CLOCK CYCLE: " + cpu.numClock.ToString()
                            + "\r\nNUMBER OF NOPCOUNT: " + cpu.numStall.ToString()
                            + "\r\nTOTAL OPERATION COUNT: " + (cpu.opCount + cpu.numStall).ToString()
                            ;
                        this.CacheBox.AppendText(performance);
                    }
                }
            }
            else
            {
                MessageBox.Show("File is not loaded.", "Warning", MessageBoxButtons.OK);
            }
        }

    }
}
