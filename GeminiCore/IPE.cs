﻿//Duke Park and Erick Grimes
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace GeminiCore
{
    #region Exceptions
    public class SyntaxError : Exception
    {
        public SyntaxError()
        {

        }
        public SyntaxError(string message)
            : base(message)
        {

        }
        public SyntaxError(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
    public class LabelNotExist : Exception
    {
        public LabelNotExist() { }
        public LabelNotExist(string message) : base(message) { }
        public LabelNotExist(string message, Exception inner) : base(message, inner) { }
    }
    public class LabelAlreadyExist : Exception
    {
        public LabelAlreadyExist() { }
        public LabelAlreadyExist(string message) : base(message) { }
        public LabelAlreadyExist(string message, Exception inner) : base(message, inner) { }
    }
    #endregion

    public class IPE
    {
        #region InstructionSetArchitecture
        private Dictionary<string, byte> ISA = new Dictionary<string, byte>(){
            {"LDA", (byte)1},
            {"STA", (byte)2},
            {"ADD", (byte)4},
            {"SUB", (byte)8},
            {"MUL", (byte)16},
            {"DIV", (byte)24},
            {"AND", (byte)12},
            {"OR", (byte)6},
            {"SHL", (byte)3},
            {"NOTA", (byte)20},
            {"BA", (byte)10},
            {"BE", (byte)5},
            {"BL", (byte)18},
            {"BG", (byte)9},
            {"NOP", (byte)17},
            {"HLT", (byte)7},
        };
        #endregion

        #region IPE PROPERTY DEFINITION
        public string FileToParse { get; set; } // File to read
        public string FileToWrite { get; set; } // g.out
        public BinaryWriter gout { get; set; }
        // All detected labels in first parsing will be stored here
        private Dictionary<string, short> LabelIndexList = new Dictionary<string,short>() ;
        private short index = 0;
        #endregion
        
        public IPE(string filename)
        {
            this.FileToParse = filename;
            this.FileToWrite = Regex.Replace(filename,@"([^\\]*)\.s$","g.out");
        }

        public void forceClosegout()
        {
            this.gout.Close();
            File.Delete(this.FileToWrite);
        }

        public void ParseFile()
        {
            var lines = File.ReadAllLines(FileToParse).ToList<string>();
            List<short> instructions = new List<short>();
            gout = new BinaryWriter(File.Open(FileToWrite, FileMode.Create));
            //FIRST PARSE FOR THE LABEL DETECTION
            foreach (var line in lines)
            {
                ParseLine(line);
            }
            //SECOND PARSE FOR THE LABEL REPLACEMENT
            foreach (var line in lines)
            {
                //Console.WriteLine(line);
                short instruct = ParseLine(line, true);
                if (instruct >= 0)
                {
                    instructions.Add(instruct);
                    gout.Write(instruct);
                    gout.Write((short)0);
                }
            }
            gout.Close();
        }

        public short ParseLine(string line, bool second = false)
        {
            if (Regex.Match(line.Trim(), @"^[a-zA-Z][a-zA-Z0-9_]*:$").Success) // CHECK LABEL (ex. main: luup:)
            {
                string label = Regex.Replace(line, @":$", "").Trim(); // REMOVE COLON
                if (!this.LabelIndexList.ContainsKey(label))
                {
                    this.LabelIndexList.Add(label, (short)(index));
                }
                else if (!second)
                {
                    // CASE OF A LABEL SHOWING UP TWICE IN FIRST PARSING
                    throw new LabelAlreadyExist(index.ToString() + ": label " + label + " already exist at index " + LabelIndexList[label].ToString());
                }
                return -1;
            }
            else if (Regex.Match(line.Trim(), @"^![.]*").Success) // Comment only case
            {
                return -1;
            }
            else if (Regex.Replace(line, @"^[\s\n\t ]*$", "").Length == 0) //IGNORE WHITE SPACES
            {
                return -1;
            }
            else
            {
                // Normal instruction parsing
                // Does not happen if not second parsing stage
                if (second)
                {
                    string[] splitedline = Regex.Split(Regex.Replace(line, @"(!.*)", "").Trim(), @"[\s\n\t ]");
                    Console.WriteLine(string.Join(" ", splitedline));

                    // NONE OF INSTRUCTION LINES ARE LONGER THAN 2 WORD
                    if (splitedline.Length > 2)
                    {
                        throw new SyntaxError(index.ToString());
                    }

                    // Initialize instruction
                    short instruct;
                    try
                    {
                        //READ OPCODES
                        instruct = this.ISA[splitedline[0].ToUpper()];
                        if (instruct == this.ISA["NOTA"] || //CASE OF NOTA NOP HLT Since they do not have argument to pass
                            instruct == this.ISA["NOP"] || 
                            instruct == this.ISA["HLT"])
                        {
                            if (splitedline.Length > 1)
                            {
                                throw new SyntaxError(index.ToString());
                            }
                            instruct = (short)((int)instruct << 9);
                        }
                        else if (instruct == this.ISA["BA"] || //CASE OF BRANCHING
                            instruct == this.ISA["BE"] || 
                            instruct == this.ISA["BL"] || 
                            instruct == this.ISA["BG"])
                        {
                            if (Regex.Match(splitedline[1], @"^(#\$|\$).*").Success) //YOU CANNOT HAVE REGISTERS OR MEMORY FOR THE ARGUMENT
                            {
                                throw new SyntaxError(index.ToString());
                            }
                            instruct = (short)((int)instruct << 9);

                            if (LabelIndexList.ContainsKey(splitedline[1]))
                            {
                                instruct += this.LabelIndexList[splitedline[1]];
                            }
                            else
                            {
                                throw new LabelNotExist(index.ToString());
                            }
                        }
                        else
                        {
                            if (!Regex.Match(splitedline[1], @"^(#\$|\$).*").Success)
                            {
                                throw new SyntaxError(index.ToString());
                            }
                            instruct = (short)((int)instruct << 1);
                            if (Regex.Match(splitedline[1], @"#").Success)
                            {
                                instruct += 1;
                            }
                            instruct = (short)((int)instruct << 8);
                            int address = Convert.ToInt16(Regex.Replace(splitedline[1], @"(#\$|\$)", "").Trim());
                            if (address > 255)
                            {
                                //Buffer overflow issue.
                               throw new InternalBufferOverflowException(index.ToString());
                            }
                            //Console.WriteLine(address);
                            instruct += (short)address;
                        }
                        //Console.WriteLine("Binary: " + Convert.ToString(instruct, 2).PadLeft(16, '0'));
                        //Console.WriteLine("Hexadecimal: " + instruct.ToString("X4") + "\n");
                        this.index++;
                        return instruct;
                    }
                    catch (KeyNotFoundException)
                    {
                        // INVALID OPCODE
                        throw new SyntaxError(index.ToString());
                    }
                }
                else
                {
                    //CASE OF FIRST PARSING
                    this.index++;
                    return 0;
                }
            }
        }
    }
}
