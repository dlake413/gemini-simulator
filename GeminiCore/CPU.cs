﻿//Duke Park and Erick Grimes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace GeminiCore
{
    //setting up the bypass machine
    //Bypass machine only lasat for one clock
    public class Bypass
    {
        public bool available = false;
        public int address = -1;
        public int value = -1;
        public void on(int address, int value)
        {
            this.address = address;
            this.value = value;
            this.available = true;
        }
        public void off()
        {
            this.address = -1;
            this.value = -1;
            this.available = false;
        }
    }
    #region Exceptions
    /*  Customized Exception Errors
     *  MemoryOverFlowException:
     *  If Memory references reaches maximum, function should throw this exception for the prevention of MemoryOverFlow.
    */
    public class MemoryOverflowException : Exception
    {
        public MemoryOverflowException()
        {

        }

        public MemoryOverflowException(string message)
            : base(message)
        {

        }

        public MemoryOverflowException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
    public class CPUHalted : Exception
    {
        public CPUHalted() { }
        public CPUHalted(string message) : base(message) { }
        public CPUHalted(string message, Exception inner) : base(message, inner) { }
    }
    #endregion
    public class Instruct
    {
        private byte[] instruction;
        public bool immediate;
        public byte opcode;
        public int address;

        public Instruct(short instruction)
        {
            this.instruction = BitConverter.GetBytes(instruction); // Divide into two bytes
            this.immediate = (this.instruction[1] % 2 == 1); // Format is 7bit operand + 1bit immediate flag. if immediate flag is on, we are guaranteed to have an odd number first 8bits.
            this.opcode = (byte)((int)this.instruction[1] >> 1); // shift right will drop immediate flag.
            this.address = (int)this.instruction[0]; // IMMEDIATE VALUE OR MEMORY ADDRESS
        }

        /* 0 : regular instruction...
         * 1 : DIV/MUL
         * 2 : STA
         * 3 : BRANCH
         * 4 : HLT
         */
        public int typeInst()
        {
            if (this.opcode == 12 || this.opcode == 16)
            {
                return 1;
            }
            else if (this.opcode == 2)
            {
                return 2;
            }
            else if (this.opcode < 20 && (this.opcode % 5 == 0 || this.opcode % 9 == 0))
            {
                return 3;
            }
            else if (this.opcode == 7)
            {
                return 4;
            }
            return 0;
        }
    }
    // CREATE LINKED-LIST style pipelining
    public class Pipeline
    {
        public short fetch;
        public Instruct inst;
        public Pipeline next;
        public int state;
        public int pc;
        public int memory;
        public int stallCount = 0;
        public bool flush = false;
        public bool executed = false;

        //WHEN PIPELINE IS CREATED, THATS WHEN FETCH ALREADY HAVE BEGUN
        public Pipeline(Memory mem, int pc = 0)
        {
            this.fetch = mem.getInstruction(pc/2);
            this.pc = pc;
            this.next = null;
            this.state = 0;
        }
        public void DECODE()
        {
            this.inst = new Instruct(fetch);
        }
        public void operate(CPU cpu)
        {
            Console.WriteLine("CURRENT ACC " + cpu.ACC);
            //DECODE
            #region DECODE
            if (state == 0)
            {
                DECODE();
                Console.WriteLine("DECODE STAGE " +  cpu.ISA[inst.opcode].Method.Name + " " + inst.immediate + " " + inst.address);
                if (inst.typeInst() == 4)
                {
                    //HALT!
                    setNextState();
                }
                else
                {                
                    //HISTORY TABLE COMES IN PLAY:
                    //
                    if (inst.typeInst() == 3)
                    {
                        if (!cpu.HistoryTable.Keys.Contains(inst.address))
                        {
                            cpu.HistoryTable.Add(inst.address, new bool[4]);
                            cpu.BranchHistory.Add(inst.address, 0);
                        }
                        if (cpu.lhMode)
                        {
                            if (cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4])
                            {
                                Console.WriteLine("We predict to take the branch");
                                this.next = new Pipeline(cpu.getMemory(), inst.address * 4);
                            }
                            else
                            {
                                Console.WriteLine("We predict not to take the branch");
                                this.next = new Pipeline(cpu.getMemory(), pc + 4);
                            }
                        }
                        else
                        {
                            this.next = new Pipeline(cpu.getMemory(), pc + 4);
                        }
                    }
                    else
                    {
                        this.next = new Pipeline(cpu.getMemory(), pc + 4);
                    }
                    if (inst.typeInst() == 1)
                    {
                        stallCount = 4;
                    }
                    else if (inst.typeInst() == 2)
                    {
                        stallCount = 1;
                    }
                    else if (inst.typeInst() == 3)
                    {
                        stallCount = 1;
                    }
                    // BYPASS MODE
                    if (cpu.bMode && !inst.immediate && cpu.bypass.available && cpu.bypass.address == inst.address)
                    {
                        Console.WriteLine("RETRIEVED FROM BYPASSING..." + cpu.bypass.value);
                        inst.immediate = true;
                        inst.address = cpu.bypass.value;
                    }
                    setNextState();
                }
            }
            #endregion
            //EXECUTE
            #region EXECUTE
            else if (state == 1)
            {
                if (!executed)
                {
                    memory = (int)cpu.ISA[inst.opcode].DynamicInvoke(inst.address, inst.immediate);
                    executed = true;
                }
                Console.WriteLine("EXECUTE STAGE " + cpu.ISA[inst.opcode].Method.Name + " " + inst.immediate + " " + inst.address + " RESULT " + memory);
                if (stallCount > 0)
                {
                    if (inst.typeInst() == 3)
                    {
                        if (cpu.lhMode)
                        {
                            Console.WriteLine("Branch History: " + Convert.ToString(cpu.BranchHistory[inst.address], 2));
                            bool actual = (memory == 0);
                            Console.WriteLine("BRANCH PREDICTION: " + cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4].ToString() + 
                                " ACTUAL: " + actual.ToString());
                        }
                        if (memory < 0) // branch was not taken
                        {
                            if (cpu.lhMode)
                            {
                                //Branch was not taken, but next pipeline is branch
                                if (cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4])
                                {
                                    cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4] = false;
                                    cpu.numStall++;
                                    
                                    Console.WriteLine("FLUSHING ALL PIPELINES");
                                    flush = true;
                                    setNextState();
                                }
                                else
                                {
                                    this.next.operate(cpu);
                                    setNextState();
                                }
                            }
                            else
                            {
                                this.next.operate(cpu);
                                setNextState();
                                cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4] = false;
                            }
                            cpu.BranchHistory[inst.address] = cpu.BranchHistory[inst.address] << 1;
                        }
                        else
                        {
                            if (cpu.lhMode)
                            {
                                //Branch was taken, but next pipeline is not taken
                                if (!cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4])
                                {
                                    cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4] = true;
                                    cpu.numStall++;
                                    Console.WriteLine("FLUSHING ALL PIPELINES");
                                    flush = true;
                                    setNextState();
                                }
                                else
                                {
                                    this.next.operate(cpu);
                                    setNextState();
                                }
                            }
                            else
                            {
                                cpu.HistoryTable[inst.address][cpu.BranchHistory[inst.address] % 4] = true;
                                cpu.numStall++;
                                Console.WriteLine("FLUSHING ALL PIPELINES");
                                flush = true;
                                setNextState();
                            }
                            cpu.BranchHistory[inst.address] <<= 1;
                            cpu.BranchHistory[inst.address] += 1;
                        }
                    }
                    else if (inst.typeInst() == 2)
                    {                        
                        //BYPASSMODE
                        if (cpu.bMode)
                        {
                            Console.WriteLine("BYPASSING VALUE... " + cpu.ACC);
                            cpu.bypass.on(inst.address, cpu.ACC);
                            this.next.operate(cpu);
                            setNextState();
                        }
                        else
                        {
                            cpu.numStall++;
                            setNextState();
                        }
                    }
                    stallCount--;
                }
                else
                {
                    if (inst.typeInst() != 4)
                    {
                        this.next.operate(cpu);
                    }
                    setNextState();
                }
            }
            #endregion
            //MEMORY
            #region MEMORY
            else if (state == 2)
            {
                Console.WriteLine("MEMORY STAGE " + cpu.ISA[inst.opcode].Method.Name + " " + inst.immediate + " " + inst.address);
                if (inst.typeInst() == 0 || inst.typeInst() == 1)
                {
                    cpu.ACC = memory;
                }
                else if (inst.typeInst() == 2)
                {
                    if (cpu.bMode)
                    {
                        cpu.bypass.off();
                    }
                    cpu.getMemory().setMemory(inst.address, cpu.ACC);
                }

                if (!flush)
                {
                    if (memory < 0)
                    {
                        cpu.ADD4();
                    }
                    this.next.operate(cpu);
                }
                else
                {
                    Console.WriteLine("BRANCH PC " + cpu.PC);
                    if (memory == 0)
                    {
                        this.next = new Pipeline(cpu.getMemory(), cpu.PC);
                    }
                    else
                    {
                        this.next = new Pipeline(cpu.getMemory(), pc + 4);
                    }
                }
                setNextState();
            }
            #endregion
        }
        public void setNext(Pipeline p)
        {
            next = p;
        }
        public int currentState()
        {
            return this.state;
        }
        public void setNextState()
        {
            this.state++;
        }
    }
    public class CPU
    {
        #region CLASS DEFINITION
        #region PROPERTY DEFINITION
        public int opCount = 0;
        public bool halt = false;
        public int A { get; set; }
        public int B { get; set; }
        public int ACC { get; set; }
        public int ZERO { get; set; }
        public int ONE { get; set; }
        public int PC { get; set; }
        public int MAR { get; set; }
        public int MDR { get; set; }
        public int TEMP { get; set; }
        public int IR { get; set; }
        public int CC { get; set; }
        private Memory memory;
        private Cache cache;
        public Dictionary<byte, Delegate> ISA; // This is where all the instruction will be stored at
        /* ALL ALU OPERATION FUNCTION WILL TAKE ADDRESS AND IMMEDIATE, WHETHER IT WOULD BE USED OR NOT.
         * USE DELEGATE INSTEAD OF SWITCH OR MASSIVE IF ELSE STATEMENT */
        private delegate int instruct(int address, bool immediate);
        #endregion
        #region PIPELINING DEFINITION
        public Dictionary<int, bool[]> HistoryTable = new Dictionary<int,bool[]>();
        public Dictionary<int, int> BranchHistory = new Dictionary<int, int>();
        public AutoResetEvent clock = new AutoResetEvent(false);
        public Pipeline pHead = null;
        public bool pMode = false;
        public bool lhMode = false;
        public bool bMode = false;
        public Bypass bypass = new Bypass();
        public int numStall = 0; // This will count the number of stalls that were installed during performance
        public int numClock = 0;
        #endregion
        //Constructor
        public CPU()
        {
            this.ZERO = 0;
            this.ONE = 1;
            // MAP OUT OPCODE AND ALU OPERATION
            #region MAPPING ISA
            this.ISA = new Dictionary<byte, Delegate>()
            {
                {1,new instruct(LDA)},
                {2,new instruct(STA)},
                {4,new instruct(ADD)},
                {8,new instruct(SUB)},
                {16,new instruct(MUL)},
                {24,new instruct(DIV)},
                {12,new instruct(AND)},
                {6,new instruct(OR)},
                {3,new instruct(SHL)},
                {20,new instruct(NOTA)},
                {10,new instruct(BA)},
                {5,new instruct(BE)},
                {18,new instruct(BL)},
                {9,new instruct(BG)},
                {17,new instruct(NOP)},
                {7,new instruct(HLT)},
            };
            #endregion
        }

        #endregion

        #region PIPELINING

        public void pipelinedOperation()
        {
            if (this.halt)
            {
                throw new CPUHalted();
            }
            if (pHead == null)
            {
                pHead = new Pipeline(memory);
            }
            pHead.operate(this);
            if (pHead.currentState() > 2)
            {
                pHead = pHead.next;
                opCount++;
            }
            numClock++;
            
        }
        public Instruct FetchInstruct(short inst)
        {
            return new Instruct(inst);
        }
        #endregion
        #region CPU OPERATION METHODS
        public void initializeMemory(List<short> Instructions)
        {
            this.memory = new Memory(Instructions);
        } 
        public void initializeCache(int blockSize, int cacheSize, bool assoc)
        {
            if (assoc)
            {
                this.cache = new AssocCache(blockSize, cacheSize);
            }
            else
            {
                this.cache = new DirectCache(blockSize, cacheSize);
            }
        }
        // printing purposes
        public string getCacheMessage()
        {
            string message = this.cache.cacheMessage;
            this.cache.cacheMessage = "";
            return message;
        }
        public Memory getMemory()
        {
            return this.memory;
        }
        
        // Reset CPU
        public void initializeCPU()
        {
            this.opCount = 0;
            this.A = 0;
            this.B = 0;
            this.PC = 0;
            this.ACC = 0;
            this.MAR = 0;
            this.MDR = 0;
            this.TEMP = 0;
            this.IR = 0;
            this.CC = 0;
            this.pHead = null;
            this.pMode = false;
            this.lhMode = false;
            this.bMode = false;
        }
        public void ADD4() // Get next PC counter.
        {
            this.PC += 4;
        }
        public short getInstruction() // Use PC register to get current instruction.
        {
            if (!halt)
            {
                try
                {
                    return this.memory.getInstruction(this.PC / 2);
                }
                catch (ArgumentOutOfRangeException)
                {
                    throw new CPUHalted();
                }
            }
            else
            {
                throw new CPUHalted();
            }
        }
        public int getCurrentOperationCount()
        {
            return this.opCount;
        }
        public void operate() // Operate current instruction
        {
            //Increase Program Counter by 2 (each instruction is 2 bytes)
            short inst = getInstruction();
            this.IR = (int)inst;
            int currentPC = this.PC;
            Instruct instruction = new Instruct(inst);
            //byte[] instruction = BitConverter.GetBytes(inst); // Divide into two bytes
            //bool immediate = (instruction[1] % 2 == 1); // Format is 7bit operand + 1bit immediate flag. if immediate flag is on, we are guaranteed to have an odd number first 8bits.
            //byte opcode = (byte)((int)instruction[1] >> 1); // shift right will drop immediate flag.
            //int address = (int)instruction[0]; // IMMEDIATE VALUE OR MEMORY ADDRESS

            int newACC = (int)this.ISA[instruction.opcode].DynamicInvoke(instruction.address, instruction.immediate); //OPERATE

            if (this.PC == currentPC) // IF CURRENT PC and PC AFTER ALU OPS IS DIFFERENT, IT MEANS IT HAS JUMPED.
            {
                ADD4();
            }

            if (instruction.typeInst() == 2)
            {
                this.memory.setMemory(instruction.address, this.ACC);
            }
            else
            {
                this.ACC = newACC;
            }
            
            this.opCount++;
            this.numClock += 4;
        }

        #region Instructions
        public int LDA(int address, bool immediate)
        {
            if (immediate)
                {
                    return address;
                }
                else
                {
                    this.cache.readCache(address, this.memory);
                    return this.memory.getMemory(address);
                }
        }
        public int STA(int address, bool immediate)
        {
            //this.memory.setMemory(address, this.ACC);
            this.cache.writeCache(address, ref this.memory, (short)this.ACC);
            return 0;
        }
        public int ADD(int address, bool immediate)
        {
            if (immediate)
            {
                return this.ACC + address;
            }
            else
            {
                this.cache.readCache(address, this.memory);
                return this.ACC + this.memory.getMemory(address);
            }
        }
        public int SUB(int address, bool immediate)
        {
            if (immediate)
            {
                return this.ACC - address;
            }
            else
            {
                this.cache.readCache(address, this.memory);
                return this.ACC - this.memory.getMemory(address);
            }
        }
        public int MUL(int address, bool immediate)
        {
            if (immediate)
            {
                return this.ACC * address;
            }
            else
            {
                this.cache.readCache(address, this.memory);
                return this.ACC * memory.getMemory(address);
            }
        }
        public int DIV(int address, bool immediate)
        {
            if (immediate)
            {
                return this.ACC / address;
            }
            else
            {
                this.cache.readCache(address, this.memory);
                return this.ACC / this.memory.getMemory(address);
            }
            
        }
        public int AND(int address, bool immediate)
        {
            if (immediate)
            {
                return this.ACC & address;
            }
            else
            {
                this.cache.readCache(address, this.memory);
                return this.ACC & this.memory.getMemory(address);
            }
        }
        public int OR(int address, bool immediate)
        {
            if (immediate)
            {
                return this.ACC | address;
            }
            else
            {
                this.cache.readCache(address, this.memory);
                return this.ACC | this.memory.getMemory(address);
            }
        }
        public int SHL(int address, bool immediate = true)
        {
            return this.ACC << 2;
        }
        public int NOTA(int address = 0, bool immediate = true)
        {
            return ~this.ACC;
        }
        public int BA(int address, bool immediate = true)
        {
            this.PC = address*4;
            return 0;
        }
        public int BE(int address, bool immediate = true)
        {
            if (this.ACC == 0)
            {
                this.PC = address * 4;
                return 0;
            }
            return -1;
        }
        public int BL(int address, bool immediate = true)
        {
            if (this.ACC < 0)
            {
                this.PC = address*4;
                return 0;
            }
            return -1;
        }
        public int BG(int address, bool immediate = true)
        {
            if (this.ACC > 0)
            {
                this.PC = address*4;
                return 0;
            }
            return -1;
        }
        public int NOP(int address = 0, bool immediate = true)
        {
            return 0;
        }
        public int HLT(int address = 0, bool immediate = true)
        {
            this.halt = true;
            return 0;
        }
        #endregion

        #endregion
    }
}