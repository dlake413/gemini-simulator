﻿//Duke Park and Erick Grimes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeminiCore
{
    public class Memory
    {
        #region CLASS DEFINITION
        private List<short> Instructions;
        private int[] memory;

        public Memory(List<short> Instructions)
        {
            this.Instructions = Instructions;
            this.memory = new int[256];
        }
        #endregion

        public void initializeMemory()
        {
            this.memory = new int[256];
        }
        public int getMemory(int address)
        {
            try
            {
                return this.memory[address];
            }
            catch (IndexOutOfRangeException)
            {
                throw new OutOfMemoryException();
            }
        }
        public void setMemory(int address, int value)
        {
            try
            {
                this.memory[address] = value;
            }
            catch (IndexOutOfRangeException)
            {
                throw new OutOfMemoryException();
            }
        }
        public List<short> getInstructions()
        {
            return this.Instructions;
        }
        public short getInstruction(int index)
        {
            //Console.WriteLine(index);
            if (index >= this.Instructions.Count)
            {
                return 3584; // halt statement
            }
            return this.Instructions.ElementAt(index);
        }
        public void addInstruction(short instruction)
        {
            this.Instructions.Add(instruction);
        }
        // nextAddress() will return the current pointer to the memory stack address, and point to next address
    }

    public abstract class Cache
    {
        public int blockSize;
        public int cacheSize;
        public string cacheMessage;

        public Cache(int blockSize, int cacheSize)
        {
            this.blockSize = blockSize;
            this.cacheSize = cacheSize;
        }
        public abstract short readCache(int address, Memory mem);
        public abstract void writeCache(int address, ref Memory mem, short write);
    }

    public class DirectCache : Cache
    {
        private short[] tagField;
        private short[,] dataBlock; // 1 ~ 2 data block will be available
        private bool[,] flagBits; // Contains valid bit[0] and modified bit(dirty bit)[1]

        public DirectCache(int blockSize, int cacheSize) : base(blockSize,cacheSize)
        {
            tagField = new short[cacheSize];
            dataBlock = new short[cacheSize, blockSize];
            flagBits = new bool[cacheSize,2];
        }

        public override short readCache(int address, Memory mem)
        {
            int parseAddress = address;
            /*
             * Address is consist of:
             * tag_info | index | block_offset
             * block_offset = log2(block_size)
             * index = log2(cache_size)
             * tag_info = rest
             */
            short tag;
            int index;
            int blockOffset;

            blockOffset = parseAddress % blockSize;
            parseAddress = parseAddress >> ((int)Math.Log(blockSize, 2));
            index = parseAddress % cacheSize;
            parseAddress = parseAddress >> ((int)Math.Log(cacheSize, 2));
            tag = (short)parseAddress;

            if (flagBits[index,0]) // VALID DATA!
            {
                if (tagField[index] != tag) // MISS: tag unmatched
                {
                    cacheMessage = "MISS: tag unmatching at " + index + " Cache tag info: " + tagField[index] + " lookup tag: " + tag;
                    if (flagBits[index, 1]) // DATA IS VALID, AND DIRTY BIT IS ON. UPDATE MEMORY. NO READ THROUGH.
                    {
                        cacheMessage += "\r\nMISS: block is dirty at " + index + " WRITE BACK BEGINS";
                        parseAddress = ((tag << (int)Math.Log(cacheSize, 2)) + index) << (int)Math.Log(blockSize, 2);
                        // all blocks in cache line will be written back to the original memory.
                        for (int i = 0; i < blockSize; i++)
                        {
                            mem.setMemory(parseAddress + i, dataBlock[index, i]);
                        }
                        flagBits[index, 1] = false;
                    } // IF NOT, IT WON'T HURT TO JUST OVERWRITE THE MEMORY
                    cacheMessage += "\r\nMISS: NO READ THROUGH from " + address + " VALUE " + mem.getMemory(address);
                    dataBlock[index, blockOffset] = (short)mem.getMemory(address);
                    tagField[index] = tag;
                }
                // Hit Case. 
            }
            else // MISS: Data invalid. NO READ THROUGH
            {
                cacheMessage = "MISS: Data is invalid at " + index + " NO READ THROUGH";
                flagBits[index, 0] = true;
                parseAddress = ((tag << (int)Math.Log(cacheSize,2)) + index) << (int)Math.Log(blockSize,2);
                for (int i = 0; i < blockSize; i++)
                {
                    dataBlock[index, i] = (short)mem.getMemory(parseAddress+i);
                }
                tagField[index] = tag;
                cacheMessage += "\r\nMISS: NO READ THROUGH from " + index + " VALUE " + mem.getMemory(address);
            }

            if (cacheMessage == "")
            {
                cacheMessage = "HIT: READ CACHE at " + index + " VALUE " + dataBlock[index, blockOffset];
            }
            return dataBlock[index, blockOffset];
        }
        public override void writeCache(int address, ref Memory mem, short write)
        {
            int parseAddress = address;
            /*
             * Address is consist of:
             * tag_info | index | block_offset
             * block_offset = log2(block_size)
             * index = log2(cache_size)
             * tag_info = rest
             */
            short tag;
            int index;
            int blockOffset;

            blockOffset = parseAddress % blockSize;
            parseAddress = parseAddress >> ((int)Math.Log(blockSize, 2));
            index = parseAddress % cacheSize;
            parseAddress = parseAddress >> ((int)Math.Log(cacheSize, 2));
            tag = (short)parseAddress;

            if (flagBits[index, 0])
            {
                if (tagField[index] == tag) // HIT. WRITE BACK
                {
                    cacheMessage = "HIT: tag matching at " + index + " WRITE BACK";
                    flagBits[index, 1] = true;
                    dataBlock[index, blockOffset] = write;
                }
                else // MISS. NO WRITE ALLOCATE
                {
                    cacheMessage = "MISS: tag unmatching at " + index + " Cache tag info: " + tagField[index] + " lookup tag: " + tag;
                    mem.setMemory(address, write);
                }
            }
            else // MISS. NO WRITE ALLOCATE
            {
                cacheMessage = "MISS: data invalid at " + index + " lookup tag: " + tag;
                mem.setMemory(address, write);
            }
        }
    }
    // I was suppose to use CacheLine for both Direct and associative, but I messed up at direct.
    // TODO FOR PROJECT 3: Change DirectCache so that it will use CacheLine instead of bunch of array.
    public class CacheLine
    {
        public short tag { get; set; }
        public short blockOffset { get; set; }
        public short[] dataBlock { get; set; }
        public bool[] flagBits = new bool[2];
        public bool LSU = false;

        public CacheLine(int blockSize)
        {
            this.dataBlock = new short[blockSize];
        }

    }
    public class AssocCache : Cache
    {
        
        private int assocSets;
        private int numSets;

        private CacheLine[] cachelines;

        public AssocCache(int blockSize, int cacheSize, int assocSets = 2) : base(blockSize,cacheSize)
        {
            this.assocSets = assocSets;
            this.numSets = cacheSize/assocSets;

            this.cachelines = new CacheLine[cacheSize];
            for (int i = 0; i < cacheSize; i++)
            {
                this.cachelines[i] = new CacheLine(blockSize);
            }
        }
        public override short readCache(int address, Memory mem)
        {
            #region Addressing Part
            int parseAddress = address;
            short tag;
            int numSet;
            int blockOffset;
            Random rand = new Random();

            blockOffset = parseAddress % blockSize;
            parseAddress = parseAddress >> ((int)Math.Log(blockSize, 2));
            numSet = parseAddress % numSets;
            parseAddress = parseAddress >> ((int)Math.Log(numSets, 2));
            tag = (short)parseAddress;
            #endregion

            //Track down how many were invalid. Used for miss case.
            int numInvalid = 0;
            #region LookUp
            for ( int i = 0; i < assocSets; i++ )
            {
                int index = numSet*2 + i;
                CacheLine line = this.cachelines[index];
                if (line.flagBits[0]) // Data is valid
                {
                    if (line.tag == tag) // Hit
                    {
                        cacheMessage = "HIT: " + numSet + "|" + i + "|" + blockOffset + " tag: " + tag + " data: " + line.dataBlock[blockOffset];
                        return line.dataBlock[blockOffset];
                    }
                }
                else
                {
                    numInvalid++;
                }
            }
            #endregion
            // Since forloop did not break on HIT, MISS!

            // READ THROUGH ADDRESS
            parseAddress = ((tag << (int)Math.Log(numSets, 2)) + numSet) << (int)Math.Log(blockSize,2);

            int insertAt;

            if (numInvalid > 0) // Not all cache is filled up. Read through on one of empty ones
            {
                insertAt = numSet*2 + (numInvalid % assocSets);
                cacheMessage = "MISS: Insert At Invalid - " + numSet + "|" + (numInvalid % assocSets) + "|" + blockOffset + " tag: " + tag + " data: " + mem.getMemory(parseAddress + blockOffset);
            }
            else // All caches are filled up already. Random Access for the Read Through
            {
                insertAt = numSet*2 + rand.Next(0, assocSets);
                cacheMessage = "MISS: Insert Randomly - " + numSet + "|" + (numInvalid % assocSets) + "|" + blockOffset + " tag: " + tag + " data: " + mem.getMemory(parseAddress + blockOffset);
            }

            // SETUP READ THOROUGH
            CacheLine newline = new CacheLine(blockSize);
            newline.tag = tag;
            newline.flagBits[0] = true;

            for (int j = 0; j < blockSize; j++)
            {
                newline.dataBlock[j] = (short)mem.getMemory(parseAddress + j);
            }

            //Before we insert anything, we need to check if the cache is dirty or not.
            //If dirty: write back. else, ignore.
            if (cachelines[insertAt].flagBits[1])
            {
                int insertAtAddress = ((cachelines[insertAt].tag << (int)Math.Log(numSets,2)) + numSets) << (int)Math.Log(blockSize,2);
                for (int j = 0; j < blockSize; j++ )
                {
                    mem.setMemory(insertAtAddress + j, cachelines[insertAt].dataBlock[j]);
                }
            }

            // SET CACHE
            cachelines[insertAt] = newline;

            return cachelines[insertAt].dataBlock[blockOffset];
        }
        public override void writeCache(int address, ref Memory mem, short write)
        {
            #region Addressing Part
            int parseAddress = address;
            short tag;
            int numSet;
            int blockOffset;
            Random rand = new Random();

            blockOffset = parseAddress % blockSize;
            parseAddress = parseAddress >> ((int)Math.Log(blockSize, 2));
            numSet = parseAddress % numSets;
            parseAddress = parseAddress >> ((int)Math.Log(numSets, 2));
            tag = (short)parseAddress;
            #endregion

            bool hit = false;
            for (int i = 0; i < assocSets; i++)
            {
                int index = numSet * 2 + i;
                CacheLine line = this.cachelines[index];
                if (line.flagBits[0]) // Data is valid
                {
                    if (line.tag == tag) // Hit
                    {
                        cacheMessage = "HIT: " + numSet + "|" + i + "|" + blockOffset + " tag: " + tag + " old/new data: " + line.dataBlock[blockOffset] + "/" + write;
                        line.dataBlock[blockOffset] = write;
                        line.flagBits[1] = true;
                        this.cachelines[index] = line;
                        hit = true;
                        break;
                    }
                }
            }
            //MISS CASE // NO WRITE ALLOCATE
            if (!hit) 
            {
                cacheMessage = "MISS";
                mem.setMemory(address, write);
            }
        }
    }
}
